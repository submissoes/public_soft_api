<?php
namespace App\Controller;

use App\Entity\Lead;
use App\Entity\Module;
use App\Repository\LeadRepository;

use App\Repository\ModuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class ModuleController extends ApiController {
    /**
     * @Route("/modules", methods="GET")
     */
    public function index(ModuleRepository $moduleRepository){
        $leads = $moduleRepository->transformAll();
        return $this->respond($leads);
    }

    /**
     * @Route("/modules/{id}/leads", methods="GET")
     */
    public function getLeadsModuleByModuleId($id,
                                             LeadRepository $leadRepository){
        try{
            if(!$id) throw new \Exception('Informe um módulo');

            $leads = $leadRepository->getByModuleId($id);// ->getLeads($id);
            return $this->respond($leads);
        }catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }


    /**
     * @Route("/modules", methods="POST")
     */
    public function create(Request $request,
                           ModuleRepository $moduleRepository,
                           EntityManagerInterface $entityManager){
        try{
            $data = json_decode($request->getContent(), true);

            $moduleRepository->validadeFields($data);

            $module = new Module();
            $module->setName($data['name']);

            $entityManager->persist($module);
            $entityManager->flush();

            return $this->respondCreated($moduleRepository->transform($module));
        }catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }

    /**
     * @Route("/modules/reorder", methods="PUT")
     */
    public function reorderModules(Request $request,
                                   ModuleRepository $moduleRepository,
                                   EntityManagerInterface $entityManager){
        try{
            $modules = json_decode($request->getContent(), true);
            foreach ($modules as $module){
                $updatedModule = $moduleRepository->find($module['id']);
                $updatedModule->setOrdered($module['ordered']);
                $entityManager->persist($updatedModule);
            }
            $entityManager->flush();

            return $this->respond($modules);
        }catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }
}

?>