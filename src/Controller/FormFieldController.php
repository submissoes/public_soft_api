<?php
namespace App\Controller;

use App\Entity\FormField;
use App\Entity\Lead;
use App\Entity\Module;
use App\Repository\FormFieldRepository;
use App\Repository\LeadRepository;

use App\Repository\ModuleRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class FormFieldController extends ApiController {
    /**
     * @Route("/form_fields/{id}", methods="GET")
     */
    public function index($id,
                          FormFieldRepository $formFieldRepository){
        if(!$id) throw new \Exception('Informe um módulo');

        $leads = $formFieldRepository->getByModuleId($id);// ->getLeads($id);
        return $this->respond($leads);
    }

    /**
     * @Route("/form_fields/{id}", methods="DELETE")
     */
    public function delete($id,
                          FormFieldRepository $formFieldRepository,
                          EntityManagerInterface $entityManager){
        if(!$id) throw new \Exception('Informe o campo do formulário');

        $formField = $formFieldRepository->find($id);// ->getLeads($id);
        $entityManager->remove($formField);
        $entityManager->flush();
        return $this->respond(true);
    }

    /**
     * @Route("/form_fields", methods="POST")
     */
    public function create(Request $request,
                           FormFieldRepository $formFieldRepository,
                           ModuleRepository $moduleRepository,
                           EntityManagerInterface $entityManager){
        try{
            $data = json_decode($request->getContent(), true);
            if(!$data) throw new \Exception('Please provide a valid request');

//            $formFieldRepository->validadeFields($data);

            $formField = new FormField;
            $formField->setLabel($data['label']);
            $formField->setType($data['type']);

            $module = $moduleRepository->find($data['moduleId']);
            $formField->setModule($module);

            $entityManager->persist($formField);
            $entityManager->flush();

            return $this->respondCreated($formFieldRepository->transform($formField));
        }catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }
}

?>