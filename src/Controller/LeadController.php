<?php
namespace App\Controller;

use App\Entity\Value;
use App\Entity\Lead;
use App\Entity\Module;
use App\Repository\FormFieldRepository;
use App\Repository\LeadRepository;

use App\Repository\ModuleRepository;
use App\Repository\ValueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class LeadController extends ApiController {
    /**
     * @Route("/leads", methods="GET")
     */
    public function index(LeadRepository $leadRepository){
        $leads = $leadRepository->getAll();
        return $this->respond($leads);
    }

    /**
     * @Route("/leads", methods="POST")
     */
    public function create(Request $request,
                           LeadRepository $leadRepository,
                           ModuleRepository $moduleRepository,
                           ValueRepository $valueRepository,
                           FormFieldRepository $formFieldRepository,
                           EntityManagerInterface $entityManager){
        try{
            $data = json_decode($request->getContent(), true);
            if(!$data) throw new \Exception('Please provide a valid request');

            $leadRepository->validadeFields($data);

            $lead = new Lead;
            $lead->setName($data['name']);
            $lead->setEmail($data['email']);
            $lead->setSurname($data['surname']);

            $module = $moduleRepository->find($data['module']);
            $lead->setModule($module);

            unset($data['name']);
            unset($data['email']);
            unset($data['surname']);
            unset($data['module']);

            foreach ($data as $key => $field ){
                $formField = $formFieldRepository->find($key);

                $value = new Value;
                $value->setLead($lead);
                $value->setValue($field);
                $value->setFormfield($formField);

                $entityManager->persist($value);
            }

            $entityManager->persist($lead);
            $entityManager->flush();

            return $this->respondCreated($leadRepository->transform($lead));
        }catch (\Exception $e){
            return $this->respondValidationError($e->getMessage());
        }
    }
}

?>