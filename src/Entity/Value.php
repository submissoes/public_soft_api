<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ValueRepository")
 */
class Value
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Lead", inversedBy="formvalues")
     * @ORM\JoinColumn(nullable=false)
     */
    private $lead;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FormField", inversedBy="value")
     * @ORM\JoinColumn(nullable=false)
     */
    private $formfield;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getLead(): ?Lead
    {
        return $this->lead;
    }

    public function setLead(?Lead $lead): self
    {
        $this->lead = $lead;

        return $this;
    }

    public function getFormfield(): ?FormField
    {
        return $this->formfield;
    }

    public function setFormfield(?FormField $formfield): self
    {
        $this->formfield = $formfield;

        return $this;
    }
}
