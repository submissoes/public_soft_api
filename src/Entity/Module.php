<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ModuleRepository")
 */
class Module
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FormField", mappedBy="module")
     */
    private $form_field;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Lead", mappedBy="module")
     */
    private $leads;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordered;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    public function __construct()
    {
        $this->form_field = new ArrayCollection();
        $this->leads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|FormField[]
     */
    public function getFormField(): Collection
    {
        return $this->form_field;
    }

    public function addFormField(FormField $formField): self
    {
        if (!$this->form_field->contains($formField)) {
            $this->form_field[] = $formField;
            $formField->setModule($this);
        }

        return $this;
    }

    public function removeFormField(FormField $formField): self
    {
        if ($this->form_field->contains($formField)) {
            $this->form_field->removeElement($formField);
            // set the owning side to null (unless already changed)
            if ($formField->getModule() === $this) {
                $formField->setModule(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Lead[]
     */
    public function getLeads(): Collection
    {
        return $this->leads;
    }

    public function addLead(Lead $lead): self
    {
        if (!$this->leads->contains($lead)) {
            $this->leads[] = $lead;
            $lead->setModule($this);
        }

        return $this;
    }

    public function removeLead(Lead $lead): self
    {
        if ($this->leads->contains($lead)) {
            $this->leads->removeElement($lead);
            // set the owning side to null (unless already changed)
            if ($lead->getModule() === $this) {
                $lead->setModule(null);
            }
        }

        return $this;
    }

    public function getOrdered(): ?int
    {
        return $this->ordered;
    }

    public function setOrdered(int $ordered): self
    {
        $this->ordered = $ordered;

        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }
}
