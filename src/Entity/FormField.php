<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormFieldRepository")
 */
class FormField
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $label;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Value", mappedBy="formfield")
     * @ORM\GeneratedValue()
     */
    private $value;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Module", inversedBy="form_field")
     * @ORM\JoinColumn(nullable=false)
     */
    private $module;


    public function __construct()
    {
        $this->value = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): self
    {
        $this->label = $label;

        return $this;
    }


    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|Value[]
     */
    public function getValue(): Collection
    {
        return $this->value;
    }

    public function addValue(Value $value): self
    {
        if (!$this->value->contains($value)) {
            $this->value[] = $value;
            $value->setFormfield($this);
        }

        return $this;
    }

    public function removeValue(Value $value): self
    {
        if ($this->value->contains($value)) {
            $this->value->removeElement($value);
            // set the owning side to null (unless already changed)
            if ($value->getFormfield() === $this) {
                $value->setFormfield(null);
            }
        }

        return $this;
    }

    public function getModule(): ?Module
    {
        return $this->module;
    }

    public function setModule(?Module $module): self
    {
        $this->module = $module;

        return $this;
    }

}
