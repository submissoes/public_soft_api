<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221011935 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form_field ADD module_id INT NOT NULL');
        $this->addSql('ALTER TABLE form_field ADD CONSTRAINT FK_D8B2E19BAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('CREATE INDEX IDX_D8B2E19BAFC2B591 ON form_field (module_id)');
        $this->addSql('ALTER TABLE lead ADD module_id INT NOT NULL');
        $this->addSql('ALTER TABLE lead ADD CONSTRAINT FK_289161CBAFC2B591 FOREIGN KEY (module_id) REFERENCES module (id)');
        $this->addSql('CREATE INDEX IDX_289161CBAFC2B591 ON lead (module_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE form_field DROP FOREIGN KEY FK_D8B2E19BAFC2B591');
        $this->addSql('DROP INDEX IDX_D8B2E19BAFC2B591 ON form_field');
        $this->addSql('ALTER TABLE form_field DROP module_id');
        $this->addSql('ALTER TABLE lead DROP FOREIGN KEY FK_289161CBAFC2B591');
        $this->addSql('DROP INDEX IDX_289161CBAFC2B591 ON lead');
        $this->addSql('ALTER TABLE lead DROP module_id');
    }
}
