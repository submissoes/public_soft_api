<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190221011516 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE value ADD formfield_id INT NOT NULL');
        $this->addSql('ALTER TABLE value ADD CONSTRAINT FK_1D77583411386F6 FOREIGN KEY (formfield_id) REFERENCES form_field (id)');
        $this->addSql('CREATE INDEX IDX_1D77583411386F6 ON value (formfield_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE value DROP FOREIGN KEY FK_1D77583411386F6');
        $this->addSql('DROP INDEX IDX_1D77583411386F6 ON value');
        $this->addSql('ALTER TABLE value DROP formfield_id');
    }
}
