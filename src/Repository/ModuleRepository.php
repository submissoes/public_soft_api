<?php

namespace App\Repository;

use App\Entity\Module;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Module|null find($id, $lockMode = null, $lockVersion = null)
 * @method Module|null findOneBy(array $criteria, array $orderBy = null)
 * @method Module[]    findAll()
 * @method Module[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ModuleRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Module::class);
    }

    // /**
    //  * @return Module[] Returns an array of Module objects
    //  */
    public function transform(Module $module)
    {
        return [
            'id'    => (int) $module->getId(),
            'name' => (string) $module->getName(),
            'ordered' => (string) $module->getOrdered(),
            'color' => (string) $module->getColor(),
        ];
    }


    public function transformAll()
    {
        $modules = $this->findBy(array(),array('ordered' => 'DESC')) ;
        $modulesArray = [];

        foreach ($modules as $module) {
            $modulesArray[] = $this->transform($module);
        }

        return $modulesArray;
    }

    public function validadeFields($data) {
        if(!$data['name']) throw new \Exception('Nome do módulo requerido');
    }

    public function getLeads($moduleId){
        $module = $this->find($moduleId);

        $leads = $module->getLeads();

        foreach ($leads as $lead) {
            var_dump($lead->getEmail());
        }
        return $leads;
    }
}
