<?php

namespace App\Repository;

use App\Entity\FormField;
use App\Entity\Lead;
use App\Entity\Value;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Lead|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lead|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lead[]    findAll()
 * @method Lead[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeadRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Lead::class);
    }

    public function transform(Lead $lead)
    {
        return [
            'id'    => (int) $lead->getId(),
            'name' => (string) $lead->getName(),
            'surname' => (string) $lead->getSurname(),
            'email' => (string) $lead->getEmail(),
            'value' => $lead->getFormvalues()->getValues()
//            'module' => $lead->getModule()->getLeads()->getValues()
        ];
    }

    public function getAll()
    {
        $leads = $this->findAll();
        return $this->transformAll($leads);
    }

    public function getByModuleId($moduleId)
    {
        $leads = $this->findBy(array('module' => $moduleId));
        return $this->transformAll($leads);
    }

    public function transformAll($leads){
        $leadsArray = [];

        foreach ($leads as $lead) {
            $leadsArray[] = $this->transform($lead);
        }
        return $leadsArray;
    }

    public function validadeFields($request) {
        if(!$request['email']) throw new \Exception('Email requerido');
        if(!$request['module']) throw new \Exception('É necessário informar um módulo');
    }

}
