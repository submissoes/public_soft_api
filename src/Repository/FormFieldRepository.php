<?php

namespace App\Repository;

use App\Entity\FormField;
use App\Entity\Module;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method FormField|null find($id, $lockMode = null, $lockVersion = null)
 * @method FormField|null findOneBy(array $criteria, array $orderBy = null)
 * @method FormField[]    findAll()
 * @method FormField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FormFieldRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, FormField::class);
    }

    public function getByModuleId($moduleId)
    {
        $fields = $this->findBy(array('module' => $moduleId));
        return $this->transformAll($fields);
    }

    public function transformAll($fields){
        $leadsArray = [];

        foreach ($fields as $field) {
            $leadsArray[] = $this->transform($field);
        }
        return $leadsArray;
    }

    public function transform(FormField $field)
    {
        return [
            'id'    => (int) $field-> getId(),
            'label'    => (string) $field->getLabel(),
            'type' => (string) $field->getType(),
        ];
    }
}
